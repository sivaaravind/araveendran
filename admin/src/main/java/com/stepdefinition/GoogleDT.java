package com.stepdefinition;

import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.github.bonigarcia.wdm.WebDriverManager;

public class GoogleDT {
	public WebDriver driver = null;
	@Given("open chrome browser and enter google Url")
	public void open_chrome_browser_and_enter_google_Url() {
		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.google.com/");

	}

	@Then("verify the google url navigation")
	public void verify_the_google_url_navigation() {
		String Googleurl = driver.getCurrentUrl();
		System.out.println(Googleurl);
	}
	    

	@Then("close the google chrome browser")
	public void close_the_google_chrome_browser() {
		driver.close();
	    
	}

	@Then("click on signin button")
	public void click_on_signin_button() {
	    driver.findElement(By.linkText("Sign in")).click();
	}
	
	@Then("click on create account button")
	public void click_on_create_account_button() {
	    driver.get("https://accounts.google.com/signup/v2/webcreateaccount?continue=https%3A%2F%2Fwww.google.com%2F&hl=en&dsh=S-1827797128%3A1611249520364899&gmb=exp&biz=false&flowName=GlifWebSignIn&flowEntry=SignUp");
	    
	    
	}
	
	@Then("enter details in Create your Google Account section")
	public void enter_details_in_Create_your_Google_Account_section(io.cucumber.datatable.DataTable dataTable) {
		List<Map<String, String>> rows = dataTable.asMaps(String.class, String.class);
		for(int i=0;i<rows.size();i++)
		{
			driver.findElement(By.id("firstName")).clear();
			driver.findElement(By.id("firstName")).sendKeys(rows.get(i).get("First name"));
			driver.findElement(By.id("lastName")).clear();
			driver.findElement(By.id("lastName")).sendKeys(rows.get(i).get("Last name"));
			driver.findElement(By.id("username")).clear();
			driver.findElement(By.id("username")).sendKeys(rows.get(i).get("User name"));
			driver.findElement(By.name("Passwd")).clear();
			driver.findElement(By.name("Passwd")).sendKeys(rows.get(i).get("Password"));
			driver.findElement(By.name("ConfirmPasswd")).clear();
			driver.findElement(By.name("ConfirmPasswd")).sendKeys(rows.get(i).get("Confirm password"));
		}
	}

}
