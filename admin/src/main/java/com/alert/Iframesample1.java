package com.alert;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.edge.EdgeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Iframesample1 {

	public static void main(String[] args) throws InterruptedException {
    WebDriverManager.edgedriver().setup();
    WebDriver driver = new EdgeDriver();
    driver.manage().timeouts().implicitlyWait(40,TimeUnit.SECONDS);
    driver.get("http://www.w3schools.com/js/tryit.asp?filename=tryjs_alert");
    Thread.sleep(3000);
    driver.switchTo().frame("iframeResult");
    Thread.sleep(3000);
    driver.findElement(By.xpath("/html/body/button")).click();
    //driver.switchTo().defaultContent();
    //driver.close();

	}

}
