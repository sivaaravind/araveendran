#Author: your.email@your.domain.com
@tag
Feature: orangehrm scenarios
  I want to use this template for my feature file

  @Orangehrm_tc1
  Scenario: opening & verifying  orangehrm browser
    Given open  browser and enter Url
    Then Verify the orangehrm navigation
    Then close the orangehrm browser
    
   @Orangehrm_tc2
  Scenario: login into orangehrm browser
    Given open  browser and enter Url
    Then Verify the orangehrm navigation
    And login into orangehrm browser
    Then close the orangehrm browser
  
  @Orangehrm_tc3
  Scenario: click on users tab and verify navigation
    Given open  browser and enter Url
    Then Verify the orangehrm navigation
    And login into orangehrm browser
    Then click on users tab and verify navigation
    Then close the orangehrm browser
    
    @Orangehrm_tc4
  Scenario: click on PIM tab and verify  navigation
    Given open  browser and enter Url
    Then Verify the orangehrm navigation
    And login into orangehrm browser
    Then click on PIM tab and verify  navigation
    
    @Orangehrm_tc5
  Scenario: click on leave tab and verify  navigation
    Given open  browser and enter Url
    Then Verify the orangehrm navigation
    And login into orangehrm browser
    Then click on leave tab and verify  navigation
    
    