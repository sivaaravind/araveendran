package com.stepdefinition;


import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class Sample {
	@Given("open a browser")
	public void open_a_browser() {
    System.out.println("open a browser");
	}
	@When("enter the Url")
	public void enter_the_Url() {
		System.out.println("enter the Url");
	}

	@Then("close the browser")
	public void close_the_browser() {
		System.out.println("close the browser");	
	}
	@When("verify the page navigation")
	public void verify_the_page_navigation() {
		System.out.println("verify the page navigation");
	}

	@Then("click on the registration")
	public void click_on_the_registration() {
		System.out.println("click on the registration");
	}

	@Then("enter the registration details")
	public void enter_the_registration_details() {
		System.out.println("enter the registration details");
	}
}
