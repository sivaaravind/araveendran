package Ecommerce.admin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Flipkart2 {
	public static void main(String[] args) {
		WebDriverManager.chromedriver().setup();// setting up chromedriver
		WebDriver driver = new ChromeDriver();// getting chrome browser
		driver.get("http://www.annauniv.edu/");// getting current url
		String expTitle = "Home - anna Univer";// giving our input as title of url(function+F12 in laptops)
		String actTitle = driver.getTitle();// Getting current title of current url
		boolean status = actTitle.contains(expTitle);// comparison
		System.out.println(status);// output display
		driver.close();// closing the browser

	}
}
