$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:src/main/java/FeatureFiles/Rediff.feature");
formatter.feature({
  "name": "Rediff signup scenarios",
  "description": "  I want to use this template for my feature file",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@Rediff"
    }
  ]
});
formatter.scenario({
  "name": "verifying the url",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Rediff"
    },
    {
      "name": "@Rediff_tc1"
    },
    {
      "name": "@smoke"
    }
  ]
});
formatter.step({
  "name": "open chrome browser and enter rediff Url",
  "keyword": "Given "
});
formatter.match({
  "location": "Rediffff.open_chrome_browser_and_enter_rediff_Url()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "verify the rediff url navigation",
  "keyword": "Then "
});
formatter.match({
  "location": "Rediffff.verify_the_rediff_url_navigation()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Rediff signup process",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Rediff"
    },
    {
      "name": "@Rediff_tc2"
    },
    {
      "name": "@smoke"
    }
  ]
});
formatter.step({
  "name": "open chrome browser and enter rediff Url",
  "keyword": "Given "
});
formatter.match({
  "location": "Rediffff.open_chrome_browser_and_enter_rediff_Url()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "verify the rediff url navigation",
  "keyword": "Then "
});
formatter.match({
  "location": "Rediffff.verify_the_rediff_url_navigation()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "enter the details  for signup process,",
  "rows": [
    {
      "cells": [
        "Fullname",
        "Rediffmail",
        "Password",
        "Retypepassword",
        "Alternate Email Address",
        "Mobilenumber"
      ]
    },
    {
      "cells": [
        "masterztvijay",
        "masterztvijay",
        "abcd@1234",
        "abcd@1234",
        "master_vijouay2021@gmail.com",
        "1269548730"
      ]
    },
    {
      "cells": [
        "eswarizesimbu",
        "eswariizesimbu",
        "klmn@4321",
        "klmn@4321",
        "eswaran_simzubu2021@gmail.com",
        "3245971036"
      ]
    },
    {
      "cells": [
        "rajiniaxmurugan",
        "rajiniaxmurugan",
        "qwer@0987",
        "qwer@0987",
        "rajini_muruozgan2018@gmail.com",
        "9687412586"
      ]
    }
  ],
  "keyword": "Then "
});
formatter.match({
  "location": "Rediffff.enter_the_details_for_signup_process(DataTable)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "enter dateof birth details",
  "rows": [
    {
      "cells": [
        "Day",
        "Month",
        "Year"
      ]
    },
    {
      "cells": [
        "25",
        "JAN",
        "1986"
      ]
    },
    {
      "cells": [
        "15",
        "FEB",
        "1993"
      ]
    },
    {
      "cells": [
        "10",
        "MAR",
        "1996"
      ]
    }
  ],
  "keyword": "Then "
});
formatter.match({
  "location": "Rediffff.enter_dateof_birth_details(DataTable)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "select gender",
  "rows": [
    {
      "cells": [
        "Gender"
      ]
    },
    {
      "cells": [
        "Male"
      ]
    },
    {
      "cells": [
        "Female"
      ]
    },
    {
      "cells": [
        "Male"
      ]
    }
  ],
  "keyword": "Then "
});
formatter.match({
  "location": "Rediffff.select_gender(DataTable)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "click on country",
  "rows": [
    {
      "cells": [
        "Country"
      ]
    },
    {
      "cells": [
        "India"
      ]
    },
    {
      "cells": [
        "Belgium"
      ]
    },
    {
      "cells": [
        "Aruba"
      ]
    }
  ],
  "keyword": "Then "
});
formatter.match({
  "location": "Rediffff.click_on_country(DataTable)"
});
formatter.result({
  "status": "passed"
});
});