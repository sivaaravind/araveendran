package Ecommerce.admin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class practiceequals3 {
	public static void main(String[] args) {
		WebDriverManager.chromedriver().setup();
		WebDriver driver = new ChromeDriver();
		driver.get("https://www.javatpoint.com/");
		String a = driver.getTitle();
		String b = driver.getCurrentUrl();
		boolean status = a.equals(b);
		System.out.println(status); 
		driver.close();
	}
}
