package Ecommerce.admin;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import io.github.bonigarcia.wdm.WebDriverManager;

public class ChildC extends MainC {
	WebDriver driver;
	String Aero = "Aircraft";

	public void teachingsubject() {

		String mainsub = "manualtesting";
		System.out.println(mainsub);
	}

	public void Selenium() {
		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();
		driver.get("http://www.annauniv.edu/");
		String expTitle = "Home - Anna University";
		String actTitle = driver.getCurrentUrl();
		System.out.println(actTitle);
		boolean status = actTitle.equals(expTitle);
		System.out.println(status);

	}

	public void rediff() {
		WebDriverManager.chromedriver().setup();
		driver = new EdgeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
		driver.get("https://www.rediff.com/");
		driver.findElement(By.linkText("Create Account")).click();
		driver.findElement(By.xpath("//*[@id=\"tblcrtac\"]/tbody/tr[3]/td[3]/input")).sendKeys("Aravindan");
		driver.findElement(By.xpath("//*[@id=\"tblcrtac\"]/tbody/tr[7]/td[3]/input[1]")).sendKeys("aerovindanrithu123");
		driver.findElement(By.xpath("//*[@id=\"tblcrtac\"]/tbody/tr[9]/td[3]/input")).sendKeys("Dscet$2004");
		driver.findElement(By.xpath("//*[@id=\"tblcrtac\"]/tbody/tr[11]/td[3]/input")).sendKeys("Dscet$2004");
		driver.findElement(By.xpath("//*[@id=\"div_altemail\"]/table/tbody/tr[1]/td[3]/input"))
				.sendKeys("aerovindan@gmail.com");
		// Select code = new Select(driver.findElement(By.id("lbl_txt")));
		// code.selectByIndex(03);;
		driver.findElement(By.id("mobno")).sendKeys("9940462142");
		Select Day = new Select(driver.findElement(By.xpath("//*[@id=\"tblcrtac\"]/tbody/tr[22]/td[3]/select[1]")));
		Day.selectByIndex(20);
		Select month = new Select(driver.findElement(By.xpath("//*[@id=\"tblcrtac\"]/tbody/tr[22]/td[3]/select[2]")));
		month.selectByIndex(10);
		Select year = new Select(driver.findElement(By.xpath("//*[@id=\"tblcrtac\"]/tbody/tr[22]/td[3]/select[3]")));
		year.selectByIndex(15);
		driver.findElement(By.xpath("//*[@id=\"tblcrtac\"]/tbody/tr[24]/td[3]/input[2]")).click();
		Select country = new Select(driver.findElement(By.id("country")));
		country.selectByIndex(10);
		driver.findElement(By.xpath("//*[@id=\"tblcrtac\"]/tbody/tr[32]/td[3]/input")).sendKeys("WQR7");
		driver.findElement(By.id("Register")).click();

	}

	public void snapdeal() {
		WebDriverManager.chromedriver().setup();
		WebDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
		driver.get("https://www.snapdeal.com/");
		Actions Signin = new Actions(driver);
		Signin.moveToElement(
				driver.findElement(By.xpath("//*[@id=\"sdHeader\"]/div[4]/div[2]/div/div[3]/div[3]/div/span[1]")))
				.build().perform();
		driver.findElement(
				By.xpath("//*[@id=\"sdHeader\"]/div[4]/div[2]/div/div[3]/div[3]/div/div/div[2]/div[2]/span[2]"))
				.click();

	}

	public void instagram() throws InterruptedException {
		WebDriverManager.chromedriver().setup();
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
		driver.get("https://www.instagram.com/accounts/emailsignup/");
		String Instagram = driver.getCurrentUrl();
		System.out.println(Instagram);
		Thread.sleep(3000);
		WebElement email = driver.findElement(By.name("emailOrPhone"));
		email.sendKeys("arovindan@gmail.com");
		WebElement Fullname = driver.findElement(By.name("fullName"));
		Fullname.sendKeys("arovindan");
		WebElement username = driver.findElement(By.name("username"));
		username.sendKeys("aerovindan");
		WebElement password = driver.findElement(By.name("password"));
		password.sendKeys("Aerocool@123");
		driver.findElement(By.xpath("//*[@id=\"react-root\"]/section/main/div/div/div[1]/div/form/div[7]/div")).click();
		Select Month = new Select(driver.findElement(By
				.xpath("//*[@id=\"react-root\"]/section/main/div/div/div[1]/div/div[4]/div/div/span/span[1]/select")));
		Month.selectByIndex(5);
		Select Day = new Select(driver.findElement(By
				.xpath("//*[@id=\"react-root\"]/section/main/div/div/div[1]/div/div[4]/div/div/span/span[2]/select")));
		Day.selectByIndex(10);
		Select Year = new Select(driver.findElement(By
				.xpath("//*[@id=\"react-root\"]/section/main/div/div/div[1]/div/div[4]/div/div/span/span[3]/select")));
		Year.selectByIndex(12);
		driver.findElement(By.xpath("//*[@id=\"react-root\"]/section/main/div/div/div[1]/div/div[6]/button")).click();

	}

}
