package com.stepdefinition;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.github.bonigarcia.wdm.WebDriverManager;

public class Xpathradio {

	public WebDriver driver = null;

	@Given("open chrome browser and enter echoten url")
	public void open_chrome_browser_and_enter_echoten_url() {
		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();
		driver.get("http://www.echoecho.com/htmlforms10.htm");
	}

	@Then("verify the echoten navigation")
	public void verify_the_echoten_navigation() {
		String echourlten = driver.getCurrentUrl();
		System.out.println(echourlten);
	}

	@Then("verify the resulting output radioboxes and do count")
	public void verify_the_resulting_List_radioboxes_and_do_count() {
		List<WebElement> Resultingoutputradioboxestable1 = driver.findElements(By.xpath(""));
		int count = Resultingoutputradioboxestable1.size();
		System.out.println(count);
	}

	@Then("close the echo urlten opened browser")
	public void close_the_echo_urlten_opened_browser() {
		driver.close();
	}
	
	@Then("verify the resulting output tabletwo radioboxes and do count")
	public void verify_the_resulting_output_tabletwo_radioboxes_and_do_count() {
		List<WebElement> Resultingoutputradioboxestable2 = driver.findElements(By.xpath("//input[@name ='group2']"));
		int count = Resultingoutputradioboxestable2.size();
		System.out.println(count);
	}
	
	@Then("verify the example radio boxes and do count")
	public void verify_the_example_radio_boxes_and_do_count() {
		List<WebElement> exampleradioboxes = driver.findElements(By.xpath("//input[@name ='radio1']"));
		int count = exampleradioboxes .size();
		System.out.println(count);
	}
	
	@Then("verify the all radio boxes in echoten url and do count")
	public void verify_the_all_radio_boxes_in_echoten_url_and_do_count() {
		List<WebElement> allradioboxes = driver.findElements(By.xpath("//input[@type ='radio']"));
		int count = allradioboxes .size();
		System.out.println(count);
	}
	

} 
