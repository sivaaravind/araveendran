package com.stepdefinition;

import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.github.bonigarcia.wdm.WebDriverManager;

public class FacebookDOB {
	public WebDriver driver = null;

	@Given("Open facebok in chrome browser and enter Url")
	public void open_facebok_in_chrome_browser_and_enter_Url() {
		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();
		driver.get("https://www.facebook.com/");

	}

	@Then("verify the facebok navigation")
	public void verify_the_facebok_navigation() {
		String facebook = driver.getCurrentUrl();
		System.out.println(facebook);

	}

	@Then("click on the create new acount button")
	public void click_on_the_create_new_acount_button() {
		driver.findElement(By.linkText("Create New Account")).click();

	}

	@Then("enter the details  for signup process")
	public void enter_the_details_for_signup_process(io.cucumber.datatable.DataTable dataTable)
			throws InterruptedException {
		List<Map<String, String>> rows = dataTable.asMaps(String.class, String.class);
		for (int i = 0; i < rows.size(); i++) 
		{
			driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
			WebElement firstname = driver.findElement(By.name("firstname"));
			firstname.sendKeys(rows.get(i).get("Firstname"));
			firstname.clear();
			Thread.sleep(1000);
			WebElement Surname = driver.findElement(By.name("lastname"));
			Surname.sendKeys(rows.get(i).get("Surname"));
			Surname.clear();
			Thread.sleep(1000);
			WebElement email = driver.findElement(By.name("reg_email__"));
			email.sendKeys(rows.get(i).get("Emailaddress"));
			email.clear();
			Thread.sleep(1000);
			WebElement NewPassword = driver.findElement(By.name("reg_passwd__"));
			NewPassword.sendKeys(rows.get(i).get("NewPassword"));
			NewPassword.clear();
		}
	}
	@Then("fill date of brith details")
	public void fill_date_of_brith_details(io.cucumber.datatable.DataTable dataTable) throws InterruptedException {
		List<Map<String, String>> rows = dataTable.asMaps(String.class, String.class);
		for (int i = 0; i < rows.size(); i++) 
			
		{
		WebElement Dates = driver.findElement(By.name("birthday_day"));
		Select Date = new Select(Dates);
		Date.selectByVisibleText(rows.get(i).get("Day"));
		Thread.sleep(1000);
		WebElement mon = driver.findElement(By.name("birthday_month"));
		Select month = new Select(mon);
		month.selectByVisibleText(rows.get(i).get("Month"));
		Thread.sleep(1000);
		WebElement yea = driver.findElement(By.name("birthday_year"));
		Select year = new Select(yea);
		year.selectByVisibleText(rows.get(i).get("Year"));
		
	}
	}
	
	
}
