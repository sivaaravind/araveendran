package com.stepdefinition;

import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.github.bonigarcia.wdm.WebDriverManager;

public class Yahoosign {
	
	public WebDriver driver =null;
	@Given("open chrome browser and enter yahoourl")
	public void open_chrome_browser_and_enter_yahoourl() {
		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();
		driver.get("https://yahoo.com");
		String actURL = driver.getCurrentUrl();
		System.out.println(actURL);
			    	}

	@Then("create new account")
	public void create_new_account() {
		driver.findElement(By.linkText("Sign in")).click();
		driver.findElement(By.xpath("//*[@id=\"createacc\"]")).click();
		
	    
	}

	@Then("Signup using the required Details")
	public void signup_using_the_required_Details(io.cucumber.datatable.DataTable dataTable) {
		List<Map<String,String>>rows = dataTable.asMaps(String.class,String.class);
		for(int i=0;i<rows.size();i++)
		{
			driver.findElement(By.name("firstName")).clear();
			driver.findElement(By.name("firstName")).sendKeys(rows.get(i).get("Firstname"));
			driver.findElement(By.name("lastName")).clear();
			driver.findElement(By.name("lastName")).sendKeys(rows.get(i).get("Surname"));
			driver.findElement(By.name("yid")).clear();
			driver.findElement(By.name("yid")).sendKeys(rows.get(i).get("Emailaddress"));
			driver.findElement(By.name("password")).clear();
			driver.findElement(By.name("password")).sendKeys(rows.get(i).get("Password"));
			driver.findElement(By.name("phone")).clear();
			driver.findElement(By.name("phone")).sendKeys(rows.get(i).get("MobileNumber"));
			
		}
	   
	}

	@Then("Enter date of birth details")
	public void enter_date_of_birth_details(io.cucumber.datatable.DataTable dataTable) {
	    List<Map<String,String>>rows1 = dataTable.asMaps(String.class,String.class);
	    for(int i = 0;i<rows1.size();i++)
	    {
	    	
	    	WebElement Mon = driver.findElement(By.name("mm"));
	    	Select month = new Select(Mon);
	    	month.selectByVisibleText(rows1.get(i).get("Month"));
	    	driver.findElement(By.name("dd")).clear();
	    	driver.findElement(By.name("dd")).sendKeys(rows1.get(i).get("Day"));
	    	driver.findElement(By.name("yyyy")).clear();
	    	driver.findElement(By.name("yyyy")).sendKeys(rows1.get(i).get("Year"));
	    	
	    }
	    
	}

	@Then("finish signup")
	public void finish_signup() {
		driver.findElement(By.name("signup")).click();
	}



}
