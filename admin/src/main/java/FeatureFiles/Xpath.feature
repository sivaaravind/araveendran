#Author: your.email@your.domain.com
@XpathTraining
Feature: Xpath scenarions
  I want to use this template for my feature file

  @Xpath_tc1
  Scenario: resulting output checkbox verification
    Given open chrome browser and enter echo url
    Then verify the echo navigation
    And verify the resulting output checkboxes and do count
    Then close the echo url opened browser

  @Xpath_tc2
  Scenario: Example checkbox verification
    Given open chrome browser and enter echo url
    Then verify the echo navigation
    And verify the Example checkboxes and do count
    Then close the echo url opened browser

  @Xpath_tc3
  Scenario: All the checkbox verification
    Given open chrome browser and enter echo url
    Then verify the echo navigation
    And verify all the checkboxes and do count
    Then close the echo url opened browser
