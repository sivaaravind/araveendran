package com.alert;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.opera.OperaDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Textboxpromptsendkecancel {

	public static void main(String[] args) throws InterruptedException {
    WebDriverManager.operadriver().setup();
    WebDriver driver = new OperaDriver();
    driver.manage().window().maximize();
    driver.manage().timeouts().implicitlyWait(40,TimeUnit.SECONDS);
    driver.get("http://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");
    Thread.sleep(3000);
    driver.switchTo().frame("iframeResult");
    Thread.sleep(3000);
    driver.findElement(By.xpath("/html/body/button")).click();
    Thread.sleep(3000);
    driver.switchTo().alert().sendKeys("ARAVINDAN");
    driver.switchTo().alert().dismiss();
   // driver.switchTo().defaultContent();
    
    
    
    //driver.close();

	}

}
