#Author: your.email@your.domain.com
@Xpathtraining2
Feature: Xpath scenarios case 2
  I want to use this template for my feature file

  @xpathradio_tc1
  Scenario: To verify resulting output radio boxes
    Given open chrome browser and enter echoten url
    Then verify the echoten navigation
    And verify the resulting output radioboxes and do count
    Then close the echo urlten opened browser

  @xpathradio_tc2
  Scenario: To verify resulting output radio boxes table2
    Given open chrome browser and enter echoten url
    Then verify the echoten navigation
    And verify the resulting output tabletwo radioboxes and do count
    Then close the echo urlten opened browser

  @xpathradio_tc3
  Scenario: To verify example radio boxes
    Given open chrome browser and enter echoten url
    Then verify the echoten navigation
    And verify the example radio boxes and do count
    Then close the echo urlten opened browser

  @xpathradio_tc4
  Scenario: To verify all the radio boxes in echoten url
    Given open chrome browser and enter echoten url
    Then verify the echoten navigation
    And verify the all radio boxes in echoten url and do count
    Then close the echo urlten opened browser
