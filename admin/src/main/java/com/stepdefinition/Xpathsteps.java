package com.stepdefinition;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.github.bonigarcia.wdm.WebDriverManager;

public class Xpathsteps {
	public WebDriver driver = null;

	@Given("open chrome browser and enter echo url")
	public void open_chrome_browser_and_enter_echo_url() {
		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();
		driver.get("http://www.echoecho.com/htmlforms09.htm");

	}

	@Then("verify the echo navigation")
	public void verify_the_echo_navigation() {
		String echourl09 = driver.getCurrentUrl();
		System.out.println(echourl09);
	}

	@Then("verify the resulting output checkboxes and do count")
	public void verify_the_resulting_output_checkboxes_and_do_count() {
		List<WebElement> checkboxes = driver.findElements(By.xpath("//input[@type = 'checkbox']"));
		int count = checkboxes.size();
		System.out.println(count);
	}

	@Then("close the echo url opened browser")
	public void close_the_echo_url_opened_browser() {
		driver.close();
	}
	
	@Then("verify the Example checkboxes and do count")
	public void verify_the_Example_checkboxes_and_do_count() {
		List<WebElement> checkboxes = driver.findElements(By.xpath("//input[@value = 'ON']"));
		int count = checkboxes.size();
		System.out.println(count);
	}


}
