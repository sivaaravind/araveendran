package com.alert;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import io.github.bonigarcia.wdm.WebDriverManager;

public class Getalertbutton2 {

	public static void main(String[] args) throws InterruptedException {
    WebDriverManager.chromedriver().setup();
    WebDriver driver = new ChromeDriver();
    driver.manage().timeouts().implicitlyWait(40,TimeUnit.SECONDS);
    driver.get("http://www.w3schools.com/js/tryit.asp?filename=tryjs_confirm");
    Thread.sleep(3000);
    driver.switchTo().frame("iframeResult");
    Thread.sleep(3000);
    driver.findElement(By.xpath("/html/body/button")).click();
    Thread.sleep(3000);
    String alertMessage = driver.switchTo().alert().getText();
    System.out.println(alertMessage);
   // driver.switchTo().defaultContent();
    
    
    
    //driver.close();

	}

}
