package com.stepdefinition;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.edge.EdgeDriver;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.github.bonigarcia.wdm.WebDriverManager;

public class WelcomeSelanium {
	public WebDriver driver =null;
	@Given("Open the edge browser and enter Url")
	public void open_the_edge_browser_and_enter_Url() {
		WebDriverManager.edgedriver().setup();
		driver = new EdgeDriver();
		driver.get("http://www.practiceselenium.com/");
			    
	}

	@Then("Verify the practice selanium website navigation")
	public void verify_the_practice_selanium_website_navigation() {
		String selaniumurl = driver.getCurrentUrl();
		System.out.println(selaniumurl);
	   
	}

	@Then("click on the welcome page")
	public void click_on_the_welcome_page() {
		driver.findElement(By.linkText("Welcome")).click();
		
	    
	}

	@Then("close the practiceselenium Website opened in the browser")
	public void close_the_practiceselenium_Website_opened_in_the_browser() {
		driver.close();
	    
	}

	@Given("click on the our passion page")
	public void click_on_the_our_passion_page() {
		driver.findElement(By.linkText("Our Passion")).click();
	    
	}

	@Given("click on the menu page")
	public void click_on_the_menu_page() {
		driver.findElement(By.linkText("Menu")).click();
	   
		
	}
	@Given("click on the lets talk tea page")
	public void click_on_the_lets_talk_tea_page() {
		driver.findElement(By.linkText("Let's Talk Tea")).click();
		
		
		
	}
	@Given("click on the checkout page")
	public void click_on_the_checkout_page() {
	    driver.findElement(By.linkText("Check Out")).click();
	}


	@Then("enter your datas {string}, {string}, {string}, {string}")
	public void enter_your_datas(String Name, String Email, String Subject, String Message) {
	   System.out.println(Name);
	   System.out.println(Email);
	   System.out.println(Subject);
	   System.out.println(Message);
	}

	@Then("click on submit button")
	public void click_on_submit_button() {
	    driver.findElement(By.xpath("//*[@id=\"form_78ea690540a24bd8b9dcfbf99e999fea\"]/div[1]/div[5]/input")).click();
	}
	



}
