package com.stepdefinition;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.github.bonigarcia.wdm.WebDriverManager;

public class UploadingFile {
	public WebDriver driver = null;

	@Given("Open browser and enter url")
	public void open_browser_and_enter_url() {
		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://imgbb.com/");

	}

	@Given("verify the url navigation")
	public void verify_the_url_navigation() {
		String upload = driver.getCurrentUrl();
		System.out.println(upload);
	}

	@Then("close the current browser")
	public void close_the_current_browser() {
		driver.close();
	}

	@Then("click on start uploading button")
	public void click_on_start_uploading_button() {
		driver.findElement(By.xpath("//*[@id=\"home-cover-content\"]/div[2]/a")).click();
	}

	@Then("upload a file")
	public void upload_a_file() throws AWTException {
		StringSelection ss = new StringSelection("C:\\Users\\LENOVO\\Desktop\\selenium.png");
	     Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);
	     Robot robot = new Robot();
	     robot.delay(250);
	     robot.keyPress(KeyEvent.VK_CONTROL);
	     robot.keyPress(KeyEvent.VK_V);
	     robot.keyRelease(KeyEvent.VK_V);
	     robot.keyRelease(KeyEvent.VK_CONTROL);
	     robot.keyPress(KeyEvent.VK_ENTER);
	     robot.delay(90);
	     robot.keyRelease(KeyEvent.VK_ENTER);
		
	}

	@Then("click on upload button")
	public void click_on_upload_button() {
		driver.findElement(By.xpath("//*[@id=\"index\"]")).click();
		
		
		
	}

	

}
