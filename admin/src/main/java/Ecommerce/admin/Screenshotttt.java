package Ecommerce.admin;

import java.io.File;
import java.io.IOException;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Screenshotttt {

	public static void main(String[] args) throws IOException {
		WebDriverManager.chromedriver().setup();// setting up chromedriver
		WebDriver driver = new ChromeDriver();// getting chrome browser
		driver.manage().window().maximize();
		driver.get("https://www.primevideo.com/detail/0MNPNBTQEYNXZHB7B41T98YCMV/ref=atv_hm_hom_c_8pZiqd_2_1");// getting current url
		File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);//storing screenshot in a filename  
		FileUtils.copyFile(scrFile, new File("D:\\Screenshot\\Scr.jpg"));// copy the screenshot file to the new location in our PC by creating screenshot folder in any driver
		
	}

}
