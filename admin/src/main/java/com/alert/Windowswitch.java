package com.alert;

import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Windowswitch{

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		WebDriverManager.chromedriver().setup();
		WebDriver driver=new ChromeDriver();
		 driver.manage().timeouts().implicitlyWait(40,TimeUnit.SECONDS);
		driver.get("https://demoqa.com/browser-windows");
		String parentwindow=driver.getWindowHandle(); //capturing parent or first window
		
		System.out.println("parent window "+parentwindow);
		
		driver.findElement(By.id("tabButton")).click();
		driver.findElement(By.id("windowButton")).click();
		driver.findElement(By.id("messageWindowButton")).click();
		Set<String> allwindows=driver.getWindowHandles();//capturing all windows 
		//boolean switchstatus=false;
		for (String eachwindowname : allwindows) {
			System.out.println(eachwindowname);
				if(!eachwindowname.equals(parentwindow)) {
				driver.switchTo().window(eachwindowname);
				System.out.println("child window "+eachwindowname);
				Thread.sleep(3000);
				String childtitle=driver.getTitle();
				System.out.println(childtitle);
			}
		}
		driver.quit();
	}

}