#Author: your.email@your.domain.com
@Rediff
Feature: Rediff signup scenarios
  I want to use this template for my feature file

  @Rediff_tc1 @smoke
  Scenario: verifying the url
    Given open chrome browser and enter rediff Url
    Then verify the rediff url navigation
    

  @Rediff_tc2 @smoke
  Scenario: Rediff signup process
    Given open chrome browser and enter rediff Url
    Then verify the rediff url navigation
    Then enter the details  for signup process,
      | Fullname        | Rediffmail      | Password  | Retypepassword | Alternate Email Address        | Mobilenumber |
      | masterztvijay     | masterztvijay   | abcd@1234 | abcd@1234      | master_vijouay2021@gmail.com   |   1269548730 |
      | eswarizesimbu   | eswariizesimbu  | klmn@4321 | klmn@4321      | eswaran_simzubu2021@gmail.com  |   3245971036 |
      | rajiniaxmurugan | rajiniaxmurugan | qwer@0987 | qwer@0987      | rajini_muruozgan2018@gmail.com |   9687412586 |
    Then enter dateof birth details
      | Day | Month | Year |
      |  25 | JAN   | 1986 |
      |  15 | FEB | 1993 |
      |  10 | MAR   | 1996 |
    Then select gender
      | Gender |
      | Male   |
      | Female |
      | Male   |
    Then click on country
      | Country |
      | India   |
      | Belgium |
      | Aruba    |
   