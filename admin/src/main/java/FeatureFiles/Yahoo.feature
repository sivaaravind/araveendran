#Author: your.email@your.domain.com
@Yahoo
Feature: Yahoo Scenarios
  I want to use this template for my feature file

  @Yahoo_tc1
  Scenario: Yahoo signup account registration
    Given open chrome browser and enter yahoourl
    Then create new account
    Then Signup using the required Details
      | Firstname | Surname | Emailaddress                   | Password  | MobileNumber |
      | master    | vijay   | master_vijouay2021@yahoo.com   | abcd@1234 |   1236987625 |
      | eswaran   | simbu   | eswaran_simzubu2021@yahoo.com  | klmn@4321 |   3694519872 |
      | rajini    | murugan | rajini_muruozgan2018@yahoo.com | qwer@0987 |   4789365421 |
    Then Enter date of birth details
      | Month | Day | Year |
      | April |  25 | 1986 |
      | May   |  15 | 1993 |
      | June  |  05 | 1996 |
    Then finish signup
 