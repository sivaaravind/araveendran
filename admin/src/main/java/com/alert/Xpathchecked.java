package com.alert;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;


public class Xpathchecked {

	public static void main(String[] args) {
		WebDriverManager.chromedriver().setup();
		WebDriver driver=new ChromeDriver();
		driver.get("http://www.echoecho.com/htmlforms09.htm");
		
		WebElement checkbox = driver.findElement(By.xpath("//*[@name='option1']"));
		System.out.println("The checkbox is selection state is - " + checkbox.isSelected());
		if(!checkbox.isSelected())
			checkbox.click();
		
		WebElement checkbox1 = driver.findElement(By.xpath("//*[@name='option2']"));
		System.out.println("The checkbox is selection state is - " + checkbox1.isSelected());
		if(!checkbox1.isSelected())
			checkbox1.click();
		
		WebElement checkbox2 = driver.findElement(By.xpath("//*[@name='option3']"));
		System.out.println("The checkbox is selection state is - " + checkbox2.isSelected());
		if(!checkbox2.isSelected())
			checkbox2.click();
		
		}

}
