package Ecommerce.admin;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Seleniumeasy2 {

	public static void main(String[] args) {
	   WebDriverManager.chromedriver().setup();
	   WebDriver driver = new ChromeDriver();
	   driver.get("https://www.seleniumeasy.com/test/basic-checkbox-demo.html");
	   driver.findElement(By.id("isAgeSelected")).click();
	   List<WebElement> multiplecheckboxes = driver.findElements(By.xpath("//input[@type='checkbox']"));
	   int countCB = multiplecheckboxes.size();
	   System.out.println(countCB);
	   for(WebElement bux : multiplecheckboxes){
		   bux.click();
	   }
	   
	}

}
