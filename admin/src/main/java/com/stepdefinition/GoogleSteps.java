package com.stepdefinition;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.github.bonigarcia.wdm.WebDriverManager;

public class GoogleSteps {
	
	public WebDriver driver = null;

	@Given("open browser and enter Url")
	public void open_browser_and_enter_Url() {
		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();
		driver.get("https://www.google.com/");

	}

	@Then("verify the google navigation")
	public void verify_the_google_navigation() {
		String Googleurl = driver.getCurrentUrl();
		System.out.println(Googleurl);
	}
	

	@Then("quit the browser")
	public void quit_the_browser() {
		driver.quit();
	}

	@Then("search any product in google")
	public void search_any_product_in_google() {
		driver.findElement(By.name("q")).sendKeys("watches");

	}

	@Then("search for {string} in google")
	public void search_for_in_google(String productname) {
		driver.findElement(By.name("q")).sendKeys(productname);
	}

	@Then("search for multiple {string} in google")
	public void search_for_multiple_in_google(String Word) {
		driver.findElement(By.name("q")).sendKeys(Word);
	    
	}
	
	@Then("signup the user with {string},{string}")
	public void signup_the_user_with(String phoneno, String  email) {
		System.out.println(phoneno);
		System.out.println(email);
	}

}
