package com.stepdefinition;

import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.github.bonigarcia.wdm.WebDriverManager;

public class Rediffff {
	
	WebDriver driver = null;
	
	@Given("open chrome browser and enter rediff Url")
	public void open_chrome_browser_and_enter_rediff_Url() {
		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://register.rediff.com/register/register.php?FormName=user_details");
		
	}

	@Then("verify the rediff url navigation")
	public void verify_the_rediff_url_navigation() {
	    String expurl = "http://register.rediff.com/register/register.php?FormName=user_details";
	    String acturl = driver.getCurrentUrl();
	    boolean status = expurl.equals(acturl);
	    System.out.println(status);
	    System.out.println(acturl);
	}
	

	@Then("enter the details  for signup process,")
	public void enter_the_details_for_signup_process(io.cucumber.datatable.DataTable dataTable) {
		List<Map<String, String>> rows = dataTable.asMaps(String.class, String.class);
		for (int i = 0; i < rows.size(); i++)
		{
			driver.findElement(By.xpath("/html/body/center/form/div/table[2]/tbody/tr[3]/td[3]/input")).clear();
			driver.findElement(By.xpath("/html/body/center/form/div/table[2]/tbody/tr[3]/td[3]/input")).sendKeys(rows.get(i).get("Fullname"));
			driver.findElement(By.xpath("/html/body/center/form/div/table[2]/tbody/tr[7]/td[3]/input[1]")).clear();
			driver.findElement(By.xpath("/html/body/center/form/div/table[2]/tbody/tr[7]/td[3]/input[1]")).sendKeys(rows.get(i).get("Rediffmail"));
			driver.findElement(By.xpath("/html/body/center/form/div/table[2]/tbody/tr[9]/td[3]/input")).clear();
			driver.findElement(By.xpath("/html/body/center/form/div/table[2]/tbody/tr[9]/td[3]/input")).sendKeys(rows.get(i).get("Password"));
			driver.findElement(By.xpath("/html/body/center/form/div/table[2]/tbody/tr[11]/td[3]/input")).clear();
			driver.findElement(By.xpath("/html/body/center/form/div/table[2]/tbody/tr[11]/td[3]/input")).sendKeys(rows.get(i).get("Retypepassword"));
			driver.findElement(By.xpath("/html/body/center/form/div/table[2]/tbody/tr[14]/td/div/table/tbody/tr[1]/td[3]/input")).clear();
			driver.findElement(By.xpath("/html/body/center/form/div/table[2]/tbody/tr[14]/td/div/table/tbody/tr[1]/td[3]/input")).sendKeys(rows.get(i).get("Alternate Email Address"));
			driver.findElement(By.xpath("//*[@id=\"mobno\"]")).clear();
			driver.findElement(By.xpath("//*[@id=\"mobno\"]")).sendKeys(rows.get(i).get("Mobilenumber"));
			
		}
	}

	@Then("enter dateof birth details")
	public void enter_dateof_birth_details(io.cucumber.datatable.DataTable dataTable) {
		List<Map<String, String>> rows2 = dataTable.asMaps(String.class, String.class);
		for (int i = 0; i < rows2.size(); i++) 
		{
			driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
			WebElement Dates = driver.findElement(By.xpath("/html/body/center/form/div/table[2]/tbody/tr[22]/td[3]/select[1]"));
			Select Day = new Select(Dates);
			Day.selectByVisibleText(rows2.get(i).get("Day"));
			WebElement Months = driver.findElement(By.xpath("/html/body/center/form/div/table[2]/tbody/tr[22]/td[3]/select[2]"));
			Select Month = new Select(Months);
			Month.selectByVisibleText(rows2.get(i).get("Month"));
			WebElement Years = driver.findElement(By.xpath("/html/body/center/form/div/table[2]/tbody/tr[22]/td[3]/select[3]"));
			Select Year = new Select(Years);
			Year.selectByVisibleText(rows2.get(i).get("Year"));
			
			
		}
	}

	@Then("select gender")
	public void select_gender(io.cucumber.datatable.DataTable dataTable) {
		List<Map<String, String>> rows3 = dataTable.asMaps(String.class, String.class);
		for (int i = 0; i < rows3.size(); i++) 
		{
			
			    if(i==1&&rows3.equals("Male"))
			     driver.findElement(By.xpath("/html/body/center/form/div/table[2]/tbody/tr[24]/td[3]/input[1]")).click();
			    if(i==2&&rows3.equals("Female"))
			     driver.findElement(By.xpath("/html/body/center/form/div/table[2]/tbody/tr[24]/td[3]/input[2]")).click();
			    }
	}

	@Then("click on country")
	public void click_on_country(io.cucumber.datatable.DataTable dataTable) {
		List<Map<String, String>> rows4 = dataTable.asMaps(String.class, String.class);
		for (int i = 0; i < rows4.size(); i++) 
		{
	    WebElement Coutry = driver.findElement(By.xpath("//*[@id=\"country\"]"));
	    Select Country = new Select(Coutry);
	    Country.selectByVisibleText(rows4.get(i).get("Country"));
		}
	}

	   



}
