#Author: your.email@your.domain.com
@Practiceselanium
Feature: to access Practiceselanium website modules
  I want to use this template for my feature file

  @Practiceselanium_tc1
  Scenario: Open the Welcome page of practiceselenium Website
    Given Open the edge browser and enter Url
    Then Verify the practice selanium website navigation
    And click on the welcome page
    Then close the practiceselenium Website opened in the browser

  @Practiceselanium_tc2
  Scenario: Open the our passion module of practiceselenium Website
    Given Open the edge browser and enter Url
    And click on the our passion page
    Then close the practiceselenium Website opened in the browser

  @Practiceselanium_tc3
  Scenario: Open the menu module of practiceselenium Website
    Given Open the edge browser and enter Url
    And click on the menu page
    Then close the practiceselenium Website opened in the browser

  @Practiceselanium_tc4
  Scenario: Open the lets talk tea module of practiceselenium Website
    Given Open the edge browser and enter Url
    And click on the lets talk tea page
    Then close the practiceselenium Website opened in the browser

  @Practiceselanium_tc5
  Scenario: Open the checkout module of practiceselenium Website
    Given Open the edge browser and enter Url
    And click on the checkout page
    Then close the practiceselenium Website opened in the browser

  @Practiceselanium_tc6
  Scenario Outline: to send us an email
    Given Open the edge browser and enter Url
    Then Verify the practice selanium website navigation
    And click on the lets talk tea page
    Then enter your datas "<Name>", "<Email>", "<Subject>", "<Message>"
    Then click on submit button
    Then close the practiceselenium Website opened in the browser

    Examples: 
      | Name       | Email             | Subject  | Message |
      | aravindan  | abc@gmail.com     | aero     | success |
      | mahesh     | mahi@gmail.com    | cse      | success |
      | prashanth  | pras123@gmail.com | saero    | success |
      | Dilli babu | xyz@ymail.com     | selanium | passed  |
