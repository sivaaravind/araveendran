package com.alert;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.edge.EdgeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Frameandalert {

	public static void main(String[] args) throws InterruptedException {
    WebDriverManager.edgedriver().setup();
    WebDriver driver = new EdgeDriver();
    driver.manage().timeouts().implicitlyWait(40,TimeUnit.SECONDS);
    driver.manage().window().maximize();
    driver.get("https://demoqa.com/alerts");
    driver.findElement(By.id("promtButton")).click();
    String a = driver.switchTo().alert().getText();
    System.out.println(a);
    Thread.sleep(3000);
    driver.switchTo().alert().sendKeys("prashanth");
    driver.switchTo().alert().accept();
    //driver.switchTo().alert().dismiss();
   // driver.switchTo().defaultContent();
    
    
    
    //driver.close();

	}

}
