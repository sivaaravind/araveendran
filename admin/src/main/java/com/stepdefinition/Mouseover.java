package com.stepdefinition;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.github.bonigarcia.wdm.WebDriverManager;

public class Mouseover {
	public WebDriver driver = null;

	@Given("open  browser and enter Url")
	public void open_browser_and_enter_Url() {
		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://opensource-demo.orangehrmlive.com/");

	}

	@Then("Verify the orangehrm navigation")
	public void verify_the_orangehrm_navigation() {
		String orangeurl = driver.getCurrentUrl();
		System.out.println(orangeurl);
	}

	@Then("close the orangehrm browser")
	public void close_the_orangehrm_browser() {
		driver.close();

	}

	@Then("login into orangehrm browser")
	public void login_into_orangehrm_browser() {
		driver.findElement(By.id("txtUsername")).sendKeys("admin");
		driver.findElement(By.id("txtPassword")).sendKeys("admin123");
		driver.findElement(By.id("btnLogin")).click();
	}

	@Then("click on users tab and verify navigation")
	public void click_on_users_tab_and_verify_navigation() throws InterruptedException {
		Actions task1 = new Actions(driver);
		task1.moveToElement(driver.findElement(By.id("menu_admin_viewAdminModule"))).build().perform();
		Thread.sleep(3000);
		Actions task2 = new Actions(driver);
		task2.moveToElement(driver.findElement(By.id("menu_admin_UserManagement"))).build().perform();
		driver.findElement(By.id("menu_admin_viewSystemUsers")).click();

	}

	@Then("click on PIM tab and verify  navigation")
	public void click_on_PIM_tab_and_verify_navigation() throws InterruptedException {
		Actions task3 = new Actions(driver);
		task3.moveToElement(driver.findElement(By.id("menu_pim_viewPimModule"))).build().perform();
		Thread.sleep(3000);
		Actions task4 = new Actions(driver);
		task4.moveToElement(driver.findElement(By.id("menu_pim_Configuration"))).build().perform();
		driver.findElement(By.id("menu_admin_pimCsvImport")).click();

	}
	@Then("click on leave tab and verify  navigation")
	public void click_on_leave_tab_and_verify_navigation() throws InterruptedException {
	    Actions task5 = new Actions(driver);
	    task5.moveToElement(driver.findElement(By.id("menu_leave_viewLeaveModule"))).build().perform();
	    Thread.sleep(3000);
	    Actions task6 = new Actions(driver);
	    task6.moveToElement(driver.findElement(By.id("menu_leave_Configure"))).build().perform();
	    driver.findElement(By.id("menu_leave_defineLeavePeriod")).click();
	    
	}

}