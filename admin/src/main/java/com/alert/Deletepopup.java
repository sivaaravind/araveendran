package com.alert;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.opera.OperaDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Deletepopup {

	public static void main(String[] args) throws InterruptedException {
    WebDriverManager.operadriver().setup();
    WebDriver driver = new OperaDriver();
    driver.manage().window().maximize();
    driver.manage().timeouts().implicitlyWait(40,TimeUnit.SECONDS);
    driver.get("http://demo.guru99.com/test/delete_customer.php");
    Thread.sleep(3000);
    driver.findElement(By.name("cusid")).sendKeys("20987645");
    driver.findElement(By.name("submit")).click();
    Thread.sleep(3000);
    String a = driver.switchTo().alert().getText();
    System.out.println(a);
    driver.switchTo().alert().accept();
    String b = driver.switchTo().alert().getText();
    System.out.println(b);
    Thread.sleep(3000);
    driver.switchTo().alert().accept();
    

	}

}
