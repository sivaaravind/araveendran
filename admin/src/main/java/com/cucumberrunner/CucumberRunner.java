package com.cucumberrunner;

import org.junit.runner.RunWith;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/main/java/FeatureFiles", // Feature File Location
		glue = { "com.stepdefinition" }, // Steps or package Location
		plugin = { "pretty", "html:target/test-output" }, // to generate different type of report
		strict = true, monochrome = true, // display the readable format
		dryRun = false, // finds the undefined steps only {not executes the other steps}
		tags = { "@Rediff"})

public class CucumberRunner {

}
