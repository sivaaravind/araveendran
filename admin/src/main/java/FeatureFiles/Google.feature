#Author: your.email@your.domain.com

@Google
Feature: Google scenarios 
  I want to use this template for my feature file
 
  @Google_tc1
  Scenario: verify the google Url
    Given open browser and enter Url
    Then verify the google navigation
    Then quit the browser
    
  @Google_tc2
  Scenario: search for any product in google
    Given open browser and enter Url
    Then verify the google navigation
    And search any product in google
    Then quit the browser
    
    @Google_tc3
  Scenario: search for particular product in google
    Given open browser and enter Url
    Then verify the google navigation
    And search for "Selenium Java" in google
    
    
    @Google_tc4
  Scenario Outline: search for multiple item in google
    Given open browser and enter Url
    Then verify the google navigation
    And search for multiple "<Word>" in google
    

    Examples: 
      | Word |
      | Aircraft   |
      | Computer  |
      | Marine    |
    
    @Google_tc5
  Scenario Outline: Register the user
    Given open browser and enter Url
    Then signup the user with "<phoneno>","<email>"
    Then quit the browser

    Examples: 
      | phoneno   | email             |
      | 123456789 | dhoni@gamil.com    |
      | 987325451 | sureshraina@gmail.com |
    
 
    