#Author: your.email@your.domain.com
@GoogleDT
Feature: Google DataTables
  I want to use this template for my feature file

  @GoogleDT_tc1
  Scenario: verify the google Url
    Given open chrome browser and enter google Url
    Then verify the google url navigation
    Then close the google chrome browser

  @GoogleDT_tc2
  Scenario: sign in process
    Given open chrome browser and enter google Url
    Then verify the google url navigation
    Then click on signin button

  @GoogleDT_tc3
  Scenario: create account process
    Given open chrome browser and enter google Url
    Then verify the google url navigation
    Then click on signin button
    Then click on create account button

  @GoogleDT_tc4
  Scenario: enter details for create account process
    Given open chrome browser and enter google Url
    Then verify the google url navigation
    Then click on create account button
    And enter details in Create your Google Account section
      | First name | Last name | User name          | Password  | Confirm password |
      | master     | vijay     | master_vijay2021   | abcd@1234 | abcd@1234        |
      | eswaran    | simbu     | eswaran_simbu2021  | klmn@4321 | klmn@4321        |
      | rajini     | murugan   | rajini_murugan2018 | qwer@0987 | qwer@0987        |
